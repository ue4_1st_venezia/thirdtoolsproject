Progetto con consegna 16/06/2022

Input:

Con il dialogo si può utilizzare lo spazio per andare avanti oppuere usare il bottone "continua";
Piccolo bug: se si preme spazio nelle options prende la prima risposta e va avanti;
Nel gioco si interagisce premendo "I";
Ci si muove sia con WASD che con le frecciette;

Asset utilizzati e dove:

Yarn Spinner -> Intro e dialogo con NPC;

DoTWeen -> Cambio colore del testo nella intro, piccola UI nel """Gioco""";

Cinemachine-> Camera che segue il player;

Fmod-> Non integrato..mi dava problemi già con l'import dei file bank per sentire i suoni, così
       ho optato per l'utilizzo dei sound base di unity;

Questo  progetto non è questo gran che e non implementa cose molto più difficili
di quelle viste a lezione. 
Soprattutto per l'Intro ho fatto un po' di magheggi strani per far si che la musica finisse prima 
del cambio scena e che nel mentre le immagini non sparissero con la fine del testo scritto.
Provando ad accedere alla LineView da codice ho scoperto che tre quarti delle cose non si possono
toccare essendo in "internal", quindi via con la fantasia per riuscire a fare quello scritto sopra. 
