using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpSword : MonoBehaviour
{
    public GameObject bubbleSprite;
    public bool activateSprite;
    public UI canvasUI;

    private void Update()
    {
        if(activateSprite == true)
        {
            if(Input.GetKeyDown(KeyCode.I))
            {
                canvasUI.OpenPanel();
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.CompareTag("Player"))
        {
            bubbleSprite.SetActive(true);
            activateSprite = true;
            bubbleSprite.transform.position = transform.position;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            bubbleSprite.SetActive(false);
            activateSprite = false;
        }
    }
}
