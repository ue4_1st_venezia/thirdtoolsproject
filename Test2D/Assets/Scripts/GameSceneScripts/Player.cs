using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Player : MonoBehaviour
{
    [Header("NormalStats")]
    public float speed = 5;

    [Header("Cinemachine")]
    public GameObject obj;
    public float distanceToObj = 15f;
    public CinemachineVirtualCameraBase initialCam;
    public CinemachineVirtualCameraBase switchCam;
    public CinemachineBrain brain;

    // Update is called once per frame
    void Update()
    {
        #region PlayerMovement
        float xMove = Input.GetAxis("Horizontal");
        transform.Translate(new Vector2(xMove, 0) * Time.deltaTime * speed);
        #endregion

        if(obj && switchCam)
        {
            if(Vector3.Distance(transform.position, obj.transform.position) < distanceToObj)
            {
                SwitchCam(switchCam);
            }
            else
            {
                SwitchCam(initialCam);
            }
        }
    }

    public void SwitchCam(CinemachineVirtualCameraBase cam)
    {
        if (brain == null || cam == null) return;

        if(brain.ActiveVirtualCamera != (ICinemachineCamera)cam)
            cam.MoveToTopOfPrioritySubqueue();
    }
}
