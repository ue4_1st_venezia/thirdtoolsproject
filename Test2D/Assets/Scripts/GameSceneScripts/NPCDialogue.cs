using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

public class NPCDialogue : MonoBehaviour
{
    //Yarn Variables
    public DialogueRunner runner;
    public YarnProject project;
    public string yarnScriptTitle;
    public GameObject dialogueContainer;
    public GameObject sprite;
    public bool activeSprite = false;
    Vector3 npc;

    public RectTransform dialogueBubble;
    public Canvas canvas;
    public CanvasScaler canvasScaler;
    public float bubbleMargin = 0.1f;

    public Vector3 bubbleOffset = new Vector3(0, 2, 0);

    public Vector3 posAndOffset { get { return transform.position + bubbleOffset; } }

    private void Start()
    {
        npc = transform.position;
    }

    void Update()
    {
        if (activeSprite == true)
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                runner.SetProject(project);
                sprite.gameObject.SetActive(false);

                dialogueContainer.SetActive(true);
                dialogueContainer.transform.position = npc;
                Dialogue();
                //Alpha();
            }
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.CompareTag("Player"))
        {
            sprite.gameObject.SetActive(true);
            activeSprite = true;
            sprite.transform.position = npc;
        }
    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.collider.CompareTag("Player"))
        {
            sprite.gameObject.SetActive(false);
            activeSprite = false;
            dialogueContainer.SetActive(false);
            runner.Clear();
        }
    }

    void Dialogue()
    {
        runner.StartDialogue(yarnScriptTitle);
    }

    //Vector2 WorldToAnchoredPos(RectTransform bubble, Vector3 worldPos, float constrainMargin = -1f)
    //{
    //    Camera cam = Camera.main;
    //    if (canvas.renderMode == RenderMode.ScreenSpaceOverlay)
    //    {
    //        cam = null;
    //    }

    //    RectTransformUtility.ScreenPointToLocalPointInRectangle(bubble.parent.GetComponent<RectTransform>(), Camera.main.WorldToScreenPoint(worldPos), cam, out Vector2 screenPos);

    //    if (constrainMargin >= 0)
    //    {
    //        bool useCanvasResolution = canvasScaler != null && canvasScaler.uiScaleMode != CanvasScaler.ScaleMode.ConstantPixelSize;
    //        Vector2 screenSize = Vector2.zero;
    //        screenSize.x = useCanvasResolution ? canvasScaler.referenceResolution.x : Screen.width;
    //        screenSize.y = useCanvasResolution ? canvasScaler.referenceResolution.y : Screen.height;

    //        var halfBubbleWidth = bubble.rect.width * 2;
    //        var halfBubbleHeight = bubble.rect.height * 2;

    //        float margin;

    //        if (screenSize.x < screenSize.y)
    //        {
    //            margin = screenPos.x * constrainMargin;
    //        }
    //        else
    //        {
    //            margin = screenPos.y * constrainMargin;
    //        }

    //        screenPos.x = Mathf.Clamp(screenPos.x, margin + halfBubbleWidth - bubble.anchorMin.x * screenSize.x, -(margin + halfBubbleWidth) - bubble.anchorMax.x * screenSize.x + screenSize.x);

    //        screenPos.y = Mathf.Clamp(screenPos.y, margin + halfBubbleHeight - bubble.anchorMin.y * screenSize.y, -(margin + halfBubbleHeight) - bubble.anchorMax.y * screenSize.y + screenSize.y);
    //    }
    //    return screenPos;
    //}


    //void Alpha()
    //{
    //    if (dialogueBubble.gameObject.activeInHierarchy)
    //    {
    //        if (this != null)
    //        {
    //            dialogueBubble.anchoredPosition = WorldToAnchoredPos(dialogueBubble, this.posAndOffset);//, bubbleMargin);
    //        }
    //    }
    //}
}
