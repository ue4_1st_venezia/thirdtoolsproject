using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UI : MonoBehaviour
{
    public GameObject questionPanel;
    public Button yesButton;
    public Button noButton;
    public Image image;
    public PickUpSword sword;

    private void Start()
    {
        questionPanel.SetActive(false);
        questionPanel.transform.localScale = Vector3.zero;
        image.gameObject.SetActive(false);
        image.gameObject.transform.localScale = Vector3.zero;

        yesButton.onClick.AddListener(() => SayYes());
        noButton.onClick.AddListener(() => SayNo());
    }

    public void OpenPanel()
    {
        questionPanel.SetActive(true);
        questionPanel.transform.DOScale(1, .5f);
    }

    public void ClosePanel()
    {
        questionPanel.transform.DOScale(0, .5f).OnComplete(() => questionPanel.SetActive(false));
    }

    void SayYes()
    {
        ClosePanel();
        image.gameObject.SetActive(true);
        image.gameObject.transform.DOScale(1, .5f);
        sword.bubbleSprite.gameObject.SetActive(false);
        Destroy(sword.gameObject);
    }

    void SayNo()
    {
        ClosePanel();
    }
}
