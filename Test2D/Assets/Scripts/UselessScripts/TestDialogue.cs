using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;
using UnityEngine.UI;

public class TestDialogue : MonoBehaviour
{
    public DialogueRunner runner;
    public string st;
    public YarnProject project;
    public Button button;
    // Start is called before the first frame update
    void Start()
    {
        runner.SetProject(project);
        button.onClick.AddListener(() => Dialogue());
    }

    void Dialogue()
    {
        runner.StartDialogue(st);
    }
}
