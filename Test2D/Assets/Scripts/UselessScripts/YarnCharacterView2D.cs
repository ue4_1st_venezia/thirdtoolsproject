using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;

public class YarnCharacterView2D : DialogueViewBase
{
    public static YarnCharacterView2D instance;
    public List<YarnCharacter2D> yarnCharacters = new List<YarnCharacter2D>();

    public YarnCharacter2D character;
    YarnCharacter2D speaker;
    public Canvas canvas;
    public CanvasScaler canvasScaler;

    public RectTransform dialogueBubble;
    public RectTransform options;

    public float bubbleMargin = 0.1f;

    private void Start()
    {
        instance = this;
        dialogueBubble = GetComponentInChildren<RectTransform>(true);
    }

    public void AddCharacters(YarnCharacter2D newCharacter)
    {
        if(!instance.yarnCharacters.Contains(newCharacter))
        {
            yarnCharacters.Add(newCharacter);
        }
    }

    public void RemoveCharacters(YarnCharacter2D removedCharacter)
    {
        if(instance.yarnCharacters.Contains(removedCharacter))
        {
            yarnCharacters.Remove(removedCharacter);
        }
    }

    public override void RunLine(LocalizedLine dialogueLine, Action onDialogueLineFinished)
    {
        string characterName = dialogueLine.CharacterName;
        

        if(!string.IsNullOrEmpty(characterName))        
            speaker = FindCharacter(characterName);        
        else        
            speaker = null;
        

        onDialogueLineFinished();
    }

    YarnCharacter2D FindCharacter(string searchName)
    {
        foreach(var character in yarnCharacters)
        {
            if(character.characterName == searchName)
            {
                return character;
            }
        }
        return null;
    }

    Vector2 WorldToAnchoredPos(RectTransform bubble, Vector3 worldPos, float constrainMargin = -1f)
    {
        Camera cam = Camera.main;
        if(canvas.renderMode == RenderMode.ScreenSpaceOverlay)
        {
            cam = null;
        }

        RectTransformUtility.ScreenPointToLocalPointInRectangle(bubble.parent.GetComponent<RectTransform>(), Camera.main.WorldToScreenPoint(worldPos), cam, out Vector2 screenPos);

        if(constrainMargin >=0)
        {
            bool useCanvasResolution = canvasScaler != null && canvasScaler.uiScaleMode != CanvasScaler.ScaleMode.ConstantPixelSize;
            Vector2 screenSize = Vector2.zero;
            screenSize.x = useCanvasResolution ? canvasScaler.referenceResolution.x : Screen.width;
            screenSize.y = useCanvasResolution ? canvasScaler.referenceResolution.y : Screen.height;

            var halfBubbleWidth = bubble.rect.width / 2;
            var halfBubbleHeight = bubble.rect.height / 2;

            float margin;

            if (screenSize.x < halfBubbleWidth)
            {
                margin = screenPos.x * constrainMargin;
            }
            else
            {
                margin = screenPos.y* constrainMargin;
            }

            screenPos.x = Mathf.Clamp(screenPos.x, margin + halfBubbleHeight - bubble.anchorMin.y * screenSize.y, -(margin + halfBubbleHeight) - bubble.anchorMax.y * screenSize.y + screenSize.y);
        }

        return screenPos;
    }

    private void Update()
    {
        if(dialogueBubble.gameObject.activeInHierarchy)
        {
            if(speaker != null)
            {
                dialogueBubble.anchoredPosition = WorldToAnchoredPos(dialogueBubble,speaker.posAndOffset,bubbleMargin);
            }
            else
            {
                dialogueBubble.anchoredPosition = WorldToAnchoredPos(dialogueBubble, character.posAndOffset, bubbleMargin);
            }
        }

        if(options.gameObject.activeInHierarchy)
        {
            options.anchoredPosition = WorldToAnchoredPos(options,character.posAndOffset,bubbleMargin);
        }
    }
}
