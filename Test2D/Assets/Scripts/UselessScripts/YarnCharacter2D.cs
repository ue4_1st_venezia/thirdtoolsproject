using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yarn.Unity;

public class YarnCharacter2D : MonoBehaviour
{
    public string characterName = "MyName";
    public Vector3 bubbleOffset = new Vector3(0, 2,0);

    public Vector3 posAndOffset { get { return transform.position + bubbleOffset; } }

    // Start is called before the first frame update
    void Start()
    {
        YarnCharacterView2D.instance.AddCharacters(this);
    }

    private void OnDestroy()
    {
        if(YarnCharacterView2D.instance != null)
        {
            YarnCharacterView2D.instance.RemoveCharacters(this);
        }
    }
}
