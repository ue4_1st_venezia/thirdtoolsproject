using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Yarn.Unity;
using DG.Tweening;
using TMPro;

public class ChangeBackGround : MonoBehaviour
{
    public Image image;
    public Image image2;
    public Image image3;
    public Image image4;
    public Image image5;

    public TextMeshProUGUI dialogueTMP;
    // Start is called before the first frame update
    void Start()
    {

        FindObjectOfType<DialogueRunner>().AddCommandHandler("FirstBG", () => FirstBG());
        FindObjectOfType<DialogueRunner>().AddCommandHandler("SecondBG", () => SecondBG());
        FindObjectOfType<DialogueRunner>().AddCommandHandler("ThirdBG", () => ThirdBG());
        FindObjectOfType<DialogueRunner>().AddCommandHandler("FourthBG", () => FourthBG());
        FindObjectOfType<DialogueRunner>().AddCommandHandler("FifthBG", () => FifthBG());
    }

    private void FirstBG()
    {
        image.DOColor(new Color(0, 0, 0), 1);
        dialogueTMP.DOColor(new Color(0.98f, 0.98f, 0.98f, 1), 0);
    }
    private void SecondBG()
    {     
        image.gameObject.SetActive(false);
        image2.gameObject.SetActive(true);
        dialogueTMP.DOColor(new Color(0.74f, 0.21f, 0.21f, 1), 0);
    }

    private void ThirdBG()
    {
        image2.gameObject.SetActive(false);
        image3.gameObject.SetActive(true);
        dialogueTMP.DOColor(new Color(0.98f, 0.98f, 0.98f, 1), 0);
    }

    private void FourthBG()
    {

        image3.gameObject.SetActive(false);
        image4.gameObject.SetActive(true);
        dialogueTMP.DOColor(new Color(0.21f, 0.74f, 0.25f, 1), 0);
    }

    private void FifthBG()
    {
        dialogueTMP.fontSize = 50;
        image4.gameObject.SetActive(false);
        image5.gameObject.SetActive(true);
        dialogueTMP.DOColor(new Color(0.29f, 1, 0.96f, 1), 0);
    }
}
