using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Yarn.Unity;

public class ChangeScene : MonoBehaviour
{
    public GameObject canvas;
    public AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        FindObjectOfType<DialogueRunner>().AddCommandHandler("LoadGame", () => LoadGame());
    }

    private void LoadGame()
    {
        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        canvas.SetActive(true);
        yield return new WaitForSeconds(15);
        SceneManager.LoadScene(1);
    }
}
